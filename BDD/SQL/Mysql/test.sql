USE pivot_table;

SELECT * FROM product_country 
WHERE id_product = "1"
AND id_country = "1"
AND category = 'vegetables'
AND amount = "9127"
AND date = '2016-12-25';

-- Avec pandas faire des tables équivalentes aux TCD du fichier.
        -- Tableau 1:
-- les produits
-- 1 pays
-- la somme du produit dans le pays
-- la somme des produits dans le pays (TCD)

SELECT `id_country` FROM country
WHERE `name` = "united states";

SELECT `id_product` FROM product;

SELECT product.`name`, country.`name`, 
product_country.`category`, SUM(product_country.`amount`)
FROM product_country
JOIN product ON product_country.`id_product` = product.`id_product`
JOIN country ON product_country.`id_country` = country.`id_country`
WHERE product_country.`id_product` in (SELECT product.`id_product` FROM product)
AND product_country.`id_country` in (SELECT country.`id_country` FROM country WHERE `name` = "united states")
GROUP BY product.`name`, country.`name`, product_country.`category`;

-- Méthode:
-- 1) sous requète SELECT
-- 2) requète principale SELECT * sans WHERE
-- 3) requète principale avec sous requète
-- 4) 1er JOIN + * remplacer par champs
-- 5) 2ème JOIN + champs id_product et id_country
-- 6) Remplacement champs id pas NAME
-- 7) GROUP BY sur les autres champs du SUM()

        -- Tableau 2:
-- les produits
-- 1 pays
-- le nombre de produit dans le pays
-- le nombre total dans le pays (TCD)

SELECT product.`name`, country.`name`, 
product_country.`category`, COUNT(product_country.`amount`)
FROM product_country
JOIN product ON product_country.`id_product` = product.`id_product`
JOIN country ON product_country.`id_country` = country.`id_country`
WHERE product_country.`id_product` in (SELECT product.`id_product` FROM product)
AND product_country.`id_country` in (SELECT country.`id_country` FROM country WHERE `name` = "france")
GROUP BY product.`name`, country.`name`, product_country.`category`;


SELECT product_country.`id_order`, product.`name` AS `product`, country.`name` AS `country`, 
product_country.`category`, product_country.`amount`, product_country.`date`
FROM product_country
JOIN product ON product_country.`id_product` = product.`id_product`
JOIN country ON product_country.`id_country` = country.`id_country`
WHERE product_country.`id_product` in (SELECT product.`id_product` FROM product)
AND product_country.`id_country` in (SELECT country.`id_country` FROM country);

SELECT product_country.`id_order`, product.`name` AS `product`, country.`name` AS `country`, 
product_country.`category`, product_country.`amount`, product_country.`date`
FROM product_country
JOIN product ON product_country.`id_product` = product.`id_product`
JOIN country ON product_country.`id_country` = country.`id_country`
WHERE product_country.`id_product` in (SELECT product.`id_product` FROM product)
AND product_country.`id_country` in (SELECT country.`id_country` FROM country WHERE `name` = "france");