-- -- read /home/antony/Exo-xlsx_to_gui/BDD/SQL/xlsx_to_gui_V2.txt

DROP DATABASE IF EXISTS xlsx_gui_V2;
CREATE DATABASE IF NOT EXISTS xlsx_gui_V2;

USE xlsx_gui_V2;

CREATE TABLE `CPNE` (
  `id_CPNE` int PRIMARY KEY NOT NULL AUTO_INCREMENT,
  `label_CPNE` varchar(255) UNIQUE NOT NULL
);

CREATE TABLE `Certificateur` (
  `id_certificateur` int PRIMARY KEY NOT NULL AUTO_INCREMENT,
  `certificateur` varchar(255) UNIQUE NOT NULL
);

CREATE TABLE `Diplome` (
  `id_diplome` int PRIMARY KEY NOT NULL AUTO_INCREMENT,
  `label_diplome` varchar(255) UNIQUE NOT NULL
);

CREATE TABLE `IDCC` (
  `id_IDCC` int PRIMARY KEY NOT NULL AUTO_INCREMENT,
  `id_CPNE` int NOT NULL,
  `IDCC` int NOT NULL
);

CREATE TABLE `RNCP` (
  `id_RNCP` int PRIMARY KEY NOT NULL AUTO_INCREMENT,
  `label_RNCP` varchar(255) UNIQUE NOT NULL,
  `id_certificateur` int NOT NULL,
  `id_diplome` int NOT NULL
);

CREATE TABLE `RNCP_CPNE` (
  `id_RNCP` int NOT NULL,
  `id_CPNE` int NOT NULL,
  `NPEC_final` int NOT NULL,
  `statut` varchar(255) NOT NULL,
  `date_of_application_NPEC` date
);

CREATE UNIQUE INDEX `RNCP_CPNE_index_0` ON `RNCP_CPNE` (`id_RNCP`, `id_CPNE`);

ALTER TABLE `IDCC` ADD FOREIGN KEY (`id_CPNE`) REFERENCES `CPNE` (`id_CPNE`);

ALTER TABLE `RNCP_CPNE` ADD FOREIGN KEY (`id_CPNE`) REFERENCES `CPNE` (`id_CPNE`);

ALTER TABLE `RNCP_CPNE` ADD FOREIGN KEY (`id_RNCP`) REFERENCES `RNCP` (`id_RNCP`);

ALTER TABLE `RNCP` ADD FOREIGN KEY (`id_certificateur`) REFERENCES `Certificateur` (`id_certificateur`);

ALTER TABLE `RNCP` ADD FOREIGN KEY (`id_diplome`) REFERENCES `Diplome` (`id_diplome`);
