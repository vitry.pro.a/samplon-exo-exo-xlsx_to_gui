import mysql.connector

def connection_mysql():
    mydb = mysql.connector.connect(
        host="localhost",
        user="antony",
        password="choupette",
        database="pivot_table"
    )
    return mydb

def cursor(mydb):
    return mydb.cursor(buffered=True)

def sql_string_s(value: list):
    val = ["%s" for x in range(1, (len(value) + 1))]
    return val


def list_to_str(liste: list):
    return ", ".join(liste)


def table_for_column_table(table: str):
    if table == "country":
        return "id_country"
    if table == "product":
        return "id_product"
    if table == "product_country":
        return "id_order"

def request_bdd_wtih_condition(cursor, table, column_table, column_condition, value_condition, unique=False):
    if unique == True:
        commande = " DISTINCT"
    else:
        commande = ""
    sql = f"SELECT{commande} `{column_table}` FROM `{table}` WHERE `{column_condition}` = %s;"
    # print("Fonction request:", sql, value_condition)
    val = ([value_condition])
    cursor.execute(sql, val)
    return cursor

def add_1_value(connector, cursor, table, column, value_condition):
    sql = f"INSERT INTO `{table}` ({column}) VALUES (%s);"
    # print("Fonction add:", sql, value_condition)
    val = ([value_condition])
    cursor.execute(sql, val)
    connector.commit()

def add_all_value(connector, cursor, table, column, value_condition):
    sql = f"INSERT INTO `{table}` ({column}) VALUES (%s);"
    # print("Fonction add:", sql, value_condition)
    val = ([value_condition])
    cursor.execute(sql, val)
    connector.commit()

def last_id(cursor):
    sql = "SELECT LAST_INSERT_ID();"
    cursor.execute(sql)
    return cursor

def insert_bdd_1_value(connector, cursor, table: str, column: str, column_condition: str, value_condition: list):
    # (mydb, mycursor, "country", "name", "name", value)
    try:
        # Insertion data dans une colonne
        if len(value_condition) == 1:
            add_1_value(connector, cursor, table, column, value_condition)
        else:
            add_all_value(connector, cursor, table, column, value_condition)
        # Récupération du dernier id
        request_last_id = last_id(cursor)
        request_id_add = request_last_id.fetchone()[0]
        return request_id_add
    except:
        column_table = table_for_column_table(table)
        # Requête SQL:
        request_select = request_bdd_wtih_condition(cursor, table, column_table, column_condition, value_condition)
        request_id_select = request_select.fetchone()[0]
        return request_id_select
    

# V1
def insert_bdd_all_value(conn, cursor, table: str, column: list, column_condition: list, value_condition: list) -> int:
    if len(value_condition) > 1:   
        val_s = sql_string_s(value_condition)
        val_s_str = list_to_str(val_s)
        column_str = list_to_str(column)
    else:
        val_s_str = value_condition[0]
        column_str = column[0]
    try:
        sql = f"INSERT INTO {table} ({column_str}) VALUES ({val_s_str});"
        # print("Fonction add:", sql)
        cursor.execute(sql, value_condition)
        conn.commit()
        id = cursor.lastrowid
    except:
        column_table = table_for_column_table(table)
        val = [value_condition[0], value_condition[1], value_condition[2], value_condition[3], value_condition[4]]
        sql = f"""  SELECT {column_table} FROM {table} 
                    WHERE {column_condition[0]} = %s 
                    AND {column_condition[1]} = %s
                    AND {column_condition[2]} = %s
                    AND {column_condition[3]} = %s
                    AND {column_condition[4]} = %s"""
        # print("Fonction request:", sql)
        cursor.execute(sql, val)
        id = cursor.fetchone()[0]
    return id

# V2 
def insert_bdd_all_value_V2(conn, cursor, table: str, column: list, column_condition: list, value_condition: list) -> int:
    if len(value_condition) > 1:   
        val_s = sql_string_s(value_condition)
        val_s_str = list_to_str(val_s)
        column_str = list_to_str(column)
    else:
        val_s_str = value_condition[0]
        column_str = column[0]
    try:
        sql = f"INSERT INTO {table} ({column_str}) VALUES ({val_s_str});"
        # print("Fonction add:", sql)
        cursor.execute(sql, value_condition)
        conn.commit()
        id = cursor.lastrowid
    except:
        column_table = table_for_column_table(table)
        sql = f"""  SELECT {column_table} FROM {table} 
                    WHERE {column_condition[0]} = %s"""
        if len(column_condition) > 1:
            val = value_condition
            for nb_cond in range(len(column_condition)):
                cond_sql = f"""  AND {column_condition[nb_cond]} = %s"""
                sql = sql + cond_sql
        else:
            val = value_condition
            sql = sql
        print("Fonction request:", sql)
        cursor.execute(sql, val)
        id = cursor.fetchone()[0]
    return id