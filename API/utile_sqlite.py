


def sql_string_s(value: list):
    val = ["?" for x in range(1, (len(value) + 1))]
    return val


def list_to_str(liste: list):
    return ", ".join(liste)


    """ Liste des colonnes BDD:
# CPNE = [id_CPNE, label_CPNE]
# Certificateur = [id_certificateur, certificateur]
# Diplome = [id_diplome, label_diplome]
# IDCC = [id_IDCC, id_CPNE, IDCC]
# NPCE = [id_NPCE, id_RNCP, id_CPNE, value_NPCE]
# RNCP = [id_RNCP, label_RNCP, id_certificateur, id_diplome]
# RNCP_CPNE = [id_RNCP, id_CPNE, NPEC_final, statut, date_of_application_NPEC]
    """
def table_for_column_table(table: str):
    if table == "CPNE":
        return "id_CPNE"
    if table == "Certificateur":
        return "id_certificateur"
    if table == "Diplome":
        return "id_diplome"
    if table == "IDCC":
        return "id_IDCC"
    if table == "NPCE":
        return "id_NPCE"
    if table == "RNCP":
        return "id_NPCE"
    if table == "RNCP_CPNE":
        return "id_RNCP, id_CPNE"
    

def insert_bdd_all_value(connexion, cursor, table: str, column: list, column_condition: list, value_condition: list) -> int:
    if len(value_condition) > 1:   
        val_s = sql_string_s(value_condition)
        val_s_str = list_to_str(val_s)
        column_str = list_to_str(column)
    else:
        val_s_str = value_condition[0]
        column_str = column[0]
    try:
        sql = f"INSERT INTO {table} ({column_str}) VALUES ({val_s_str});"
        print("Fonction add:", sql)
        cursor.execute(sql, value_condition)
        connexion.commit()
        id = cursor.lastrowid
    except:
        nb_list: int = 0
        column_table = table_for_column_table(table)
        sql = f"""  SELECT {column_table} FROM {table} 
                    WHERE {column_condition[nb_list]} = ?"""
        if len(column_condition) > 1:
            val = value_condition
            for nb_cond in range(1, len(column_condition)):
                cond_sql = f""" AND {column_condition[nb_cond]} = ?"""
                sql = sql + cond_sql
        else:
            print(column_condition, value_condition)
            if len(value_condition) > 1:
                val = [value_condition[1]]
            else:
                val = value_condition
                sql = sql
            print(val)
        print("Fonction request:", sql)
        cursor.execute(sql, val)
        id = cursor.fetchone()[0]
    return id


def insert_bdd_debug(connexion, cursor, table: str, column: list, column_condition: list, value_condition: list):
    nb_list: int = 0
    column_table = table_for_column_table(table)
    sql = f"""  SELECT {column_table} FROM {table} 
        WHERE {column_condition[nb_list]} = ?"""
    if len(column_condition) > 1:
        val = value_condition
        for nb_cond in range(1, len(column_condition)):
            cond_sql = f""" AND {column_condition[nb_cond]} = ?"""
            sql = sql + cond_sql
    else:
        if len(value_condition) == 1:
            val = value_condition
        else:
            val = [value_condition[1]]
            sql = sql
    print("insert:", "/ table:", table, "/ column:", column, "/ column_cond:", column_condition, "/ value:", [x for x in value_condition], "/ value_type:", [type(x) for x in value_condition])
    print("select:", "/ value_condition:", value_condition, "/ val:", val)
    print("sql:", sql)
    print()
    return 55


# def insertion_data(dataframe):
#     list_table = ["product", "country", "product_country"]
#     list_column_product = ["id_product", "name"]
#     list_column_country = ["id_country", "name"]
#     list_column_product_country = ["id_order", "id_product", "id_country", "category", "amount", "date"]
#     for index in range(len(dataframe)):
#         print("-" * 25, "Insertion Data:", "-" * 25)
#         # Insertion country
#         value_country = dataframe.iloc[index, 4]
#         print("Row: ", index, " / Country: ", value_country)
#         # Valeur dans fonction:
#         # print(list_table[1], list_column_country[1], list_column_country[1])
#         id_country = insert_bdd_1_value(mydb, mycursor, list_table[1], list_column_country[1], list_column_country[1], value_country)
#         mydb.commit()
#         print("Id_country: ", id_country)
#         print()
#         # Insertion product
#         value_product = dataframe.iloc[index, 0]
#         print("Row: ", index, " / Product: ", value_product)
#         # Valeur dans fonction:
#         # print(list_table[0], list_column_product[1], list_column_product[1])
#         id_product = insert_bdd_1_value(mydb, mycursor, list_table[0], list_column_product[1], list_column_product[1], value_product)
#         mydb.commit()
#         print("Id_product: ", id_product)
#         print()
#         # Insertion product_country
#         list_column_condition = list_column_product_country[1:]
#         list_value = [id_product, id_country, dataframe.iloc[index, 1], int(dataframe.iloc[index, 2]), dataframe.iloc[index, 3]]
#         print("Row:", index, "/ list_column_condition:", list_column_condition)
#         print("Champs:", list_value)
#         # Valeur dans fonction: ["product_country", [list_column_product_country], [list_column_condition], list_value]
#             # table: str, column: list, column_condition: list, value_condition: list
#         id_order = insert_bdd_all_value(mydb, mycursor, list_table[2], list_column_product_country[1:], list_column_condition, list_value)
#         mydb.commit()
#         print("Id_order: ", id_order)
#         print()