import pandas as pd
import unidecode
import sqlite3
import time
from extract_file import extract_data_excel
# from utile_mysql import request_bdd_wtih_condition, add_1_value, last_id, insert_bdd_1_value, insert_bdd_all_value
from utile_sqlite import insert_bdd_debug, insert_bdd_all_value

path_test = 'DATA/Referentiel_copy.xlsx'
path = 'DATA/Referentiel-des-NPEC.xlsx'
database = '/home/antony/SQLite_DB/xlsx_gui_V1.db'

# sqliteConnection = sqlite3.connect(database)
# cursor = sqliteConnection.cursor()

def formatage_df(dataframe: pd.DataFrame):
    def tuple_in_column(dataframe: pd.DataFrame):
        for column in dataframe.columns.values:
            column_str = column.split('*')[0]
            column_str = column_str.replace(' ', '_')
            column_str = column_str.lower()
            column_str = unidecode.unidecode(column_str)
            if column_str == "date_d'applicabilite_des_npec":
                column_str = "date_d'application_npec"
            dataframe.rename(columns={column: column_str}, inplace=True)
            # print(column_str, column)
        return dataframe
    # Etape 0 : Fomatage des colonnes
    dataframe = tuple_in_column(dataframe)
    for column in dataframe.columns.values:
        # Etape 1 : convertir le format de la date
        if column == "date_d'application_npec":
            dataframe[column] = dataframe[column].dt.strftime('%Y-%m-%d') # AAAA-MM-JJ
        # Etape 2: convertir les strings en lowercase
        if column == 'cpne' or column == 'libelle_de_la_formation' or column == "certificateur" or column == "cpne":
            dataframe[column] = dataframe[column].str.normalize('NFKD').str.encode('ascii', errors='ignore').str.decode('utf-8')
            if column == 'libelle_de_la_formation' or column == "certificateur":
                dataframe[column] = dataframe[column].str.lower()
        # Etape 3: convertir amouts en int
        if column == "code_cpne" or column == "npec_final" or column == "code_cpne" or column == "idcc":
            # print(column)
            if column == "code_cpne" or column == "code_cpne" or column == "idcc":
                dataframe[column] = dataframe[column].astype(str).astype(int)
            else:
                dataframe[column] = dataframe[column].astype(float).astype(int)
    return dataframe


def insertion_data_onglet_3(connexion, cursor, dataframe):
    list_table = ["CPNE", "Certificateur", "Diplome", "IDCC", "NPCE", "RNCP", "RNCP_CPNE"]
    list_column_cpne = ["id_CPNE", "label_CPNE"]
    list_column_Certificateur = ["id_certificateur", "certificateur"]
    list_column_Diplome = ["id_diplome", "label_diplome"]
    list_column_RNCP = ["id_RNCP", "label_RNCP", "id_certificateur", "id_diplome"]
    list_column_RNCP_CPNE = ["id_RNCP", "id_CPNE", "NPEC_final", "statut", "date_of_application_NPEC"]
    for index in range(len(dataframe)):
        print(">" * 25, "Insertion Data:", "<" * 25)
        # Récupération data par colonne
        value_code_rncp = dataframe.iloc[index, 0]
        value_label_rncp = dataframe.iloc[index, 1]
        value_label_certificateur = dataframe.iloc[index, 2]
        value_label_diplome = dataframe.iloc[index, 3]
        value_code_cpne = dataframe.iloc[index, 4]
        value_label_cpne = dataframe.iloc[index, 5]
        value_npec_final = dataframe.iloc[index, 6]
        value_statut = dataframe.iloc[index, 7]
        value_date_npec = dataframe.iloc[index, 8]
        print("Row: ", index, " / Code_CPNE: ", int(value_code_cpne), " / Label_cpne: ", value_label_cpne)
        print("Row: ", index, " / Label_certificateur: ", value_label_certificateur, " / Label_diplome: ", value_label_diplome)
        print("Row: ", index, " / Code_RNCP: ", value_code_rncp, " / Label_rncp: ", value_label_rncp)
        print("Row: ", index, " / NPEC_final: ", int(value_npec_final), " / Statut: ", value_statut, " / Date_NPEC: ", value_date_npec)
        print()
        # Insertion dans table CPNE = [id_CPNE, label_CPNE]
        value_cpne = [int(value_code_cpne), value_label_cpne]
        id_cpne = insert_bdd_all_value(connexion, cursor, list_table[0], list_column_cpne, [list_column_cpne[1]], value_cpne)
        connexion.commit()
        # Insertion dans table Certificateur = ["id_certificateur", "certificateur"]
        value_certificateur = [value_label_certificateur]
        id_certificateur = insert_bdd_debug(connexion, cursor, list_table[1], list_column_Certificateur[1:], [list_column_Certificateur[1]], value_certificateur)
        connexion.commit()
        # Insertion dans table Diplome = [id_diplome, label_diplome]
        value_diplome = [value_label_diplome]
        id_diplome = insert_bdd_all_value(connexion, cursor, list_table[2], list_column_Diplome[1:], [list_column_Diplome[1]], value_diplome)
        connexion.commit()
        # Insertion dans table RNCP = [id_RNCP, label_RNCP, id_certificateur, id_diplome]
        value_rncp = [value_code_rncp, value_label_rncp, int(id_certificateur), int(id_diplome)]
        id_rncp = insert_bdd_all_value(connexion, cursor, list_table[5], list_column_RNCP, [list_column_RNCP[1]], value_rncp)
        connexion.commit()
        # Insertion dans table RNCP_CPNE = [id_RNCP, id_CPNE, NPEC_final, statut, date_of_application_NPEC]
        value_rncp_cpne = [int(id_rncp), int(id_cpne), int(value_npec_final), value_statut, value_date_npec]
        # id_rncp_cpne = insert_bdd_debug(connexion, cursor, list_table[6], list_column_RNCP_CPNE[:-3], list_column_RNCP_CPNE[:-3], value_rncp_cpne)
        # connexion.commit()
        print("id_cpne: ", id_cpne, 
            "/ id_certificateur: ", id_certificateur,
            "/ id_diplome: ", id_diplome,
            "/ id_rncp: ", id_rncp,
            "/ id_rncp_cpne: ")#, id_rncp_cpne)
        print()


    """ Liste des colonnes BDD:
# CPNE = [id_CPNE, label_CPNE]
# Certificateur = [id_certificateur, certificateur]
# Diplome = [id_diplome, label_diplome]
# IDCC = [id_IDCC, id_CPNE, IDCC]
# NPCE = [id_NPCE, id_RNCP, id_CPNE, value_NPCE]
# RNCP = [id_RNCP, label_RNCP, id_certificateur, id_diplome]
# RNCP_CPNE = [id_RNCP, id_CPNE, NPEC_final, statut, date_of_application_NPEC]
    """


def insertion_data_onglet_4(connexion, cursor, dataframe):
    list_table = ["CPNE", "Certificateur", "Diplome", "IDCC", "NPCE", "RNCP", "RNCP_CPNE"]
    list_column_cpne = ["id_CPNE", "label_CPNE"]
    list_column_idcc = ["id_IDCC", "id_CPNE", "IDCC"]
    for index in range(len(dataframe)):
        print(">" * 25, "Insertion Data:", "<" * 25)
        # Récupération data par colonne
        value_code_cpne = dataframe.iloc[index, 0]
        value_label_cpne = dataframe.iloc[index, 1]
        value_idcc = dataframe.iloc[index, 2]
        print("Row: ", index, " / Code: ", int(value_code_cpne), " / Label: ", value_label_cpne, " / IDCC: ", int(value_idcc))
        # Insertion dans table CPNE = [id_CPNE, label_CPNE]
        value_cpne = [int(value_code_cpne), value_label_cpne]
        id_code_cpne = insert_bdd_all_value(connexion, cursor, list_table[0], list_column_cpne[:], list_column_cpne, value_cpne)
        connexion.commit()
        # Insertion dans table IDCC = [id_IDCC, id_CPNE, IDCC]
        value_IDCC = [int(value_code_cpne),  int(value_idcc)]
        id_idcc = insert_bdd_all_value(connexion, cursor, list_table[3], list_column_idcc[1:], [list_column_idcc[0]], value_IDCC)
        connexion.commit()
        print("id_CPNE: ", id_code_cpne, "/ id_IDCC: ", id_idcc)
        print()

if __name__ == "__main__":
    start = time.time()
    print(f"{' Début du Script: ':#^78}")
    print("-" * 25, "Extraction Data Feuille Excel:", "-" * 25)
    s_extract = time.time()
    # df_onglet_2 = extract_data_excel(path, 1, header=[3, 4])
    print("Onglet 3 en cours...")
    df_onglet_3 = extract_data_excel(path_test, 2, header=3)
    print("Onglet 4 en cours...")
    # df_onglet_4 = extract_data_excel(path_test, 3, header=2)
    e_extract = time.time()
    elapsed_extract = e_extract - s_extract
    print(f'Temps d\'exécution Extraction : {elapsed_extract:5.2f}ms')
    
    print("-" * 25, "Formatage Data Feuille Excel:", "-" * 25)
    s_format = time.time()
    print("Onglet 3 en cours...")
    formatage_df(df_onglet_3)
    print("Onglet 4 en cours...")
    # formatage_df(df_onglet_4)
    e_format = time.time()
    elapsed_format = e_format - s_format
    print(f'Temps d\'exécution Formatage : {elapsed_format:5.2f}ms')
    
    print("-" * 25, "Connection SQLite:", "-" * 25)
    try:
        sqliteConnection = sqlite3.connect(database)
        cursor = sqliteConnection.cursor()
        print("Database created and Successfully Connected to SQLite")
    
        print("-" * 25, "Insertion Data Feuille Excel:", "-" * 25)
        s_ong3 = time.time()
        print("Onglet 3 en cours...")
        insertion_data_onglet_3(sqliteConnection, cursor, df_onglet_3)
        e_ong3 = time.time()
        elapsed_ong3 = e_ong3 - s_ong3
        print(f'Temps d\'exécution Insertion onglet 2 : {elapsed_ong3:5.2f}ms')
        
        s_ong4 = time.time()
        print("Onglet 4 en cours...")
        # insertion_data_onglet_4(sqliteConnection, cursor, df_onglet_4)
        e_ong4 = time.time()
        elapsed_ong4 = e_ong4 - s_ong4
        print(f'Temps d\'exécution Insertion onglet 4 : {elapsed_ong4:5.2f}ms')
    
        cursor.close()
    except sqlite3.Error as error:
        print("Error while connecting to sqlite", error)
    finally:
        if sqliteConnection:
            sqliteConnection.close()
            print("The SQLite connection is closed")
    end = time.time()
    elapsed = end - start
    print()
    print(f'Récapitulatif Temps d\'exécution :')
    print(f'Temps d\'exécution Extraction : {elapsed_extract:5.2f}ms')
    print(f'Temps d\'exécution Formatage : {elapsed_format:5.2f}ms')
    try:
        print(f'Temps d\'exécution Insertion onglet 3 : {elapsed_ong3:5.2f}ms')
    except NameError:
        pass
    try:
        print(f'Temps d\'exécution Insertion onglet 4 : {elapsed_ong4:5.2f}ms')
    except NameError:
        pass
    finally:
        print(f'Temps d\'exécution Total : {elapsed:5.2f}ms')
        print(f"{' Fin du Script: ':#^78}")


# Récapitulatif Temps d'exécution :
# Temps d'exécution Extraction :  1.1e+02s
# Temps d'exécution Formatage :      5.5s
# Temps d'exécution Insertion onglet 3 :     0.17s
# Temps d'exécution Insertion onglet 4 :      1.2ms
# Temps d'exécution Total :  1.2e+02s

# Récapitulatif Temps d'exécution :
# Temps d'exécution Extraction :  1.2e+02ms
# Temps d'exécution Formatage :      5.8ms
# Temps d'exécution Insertion onglet 3 :     0.18ms
# Temps d'exécution Insertion onglet 4 :      1.6ms
# Temps d'exécution Total :  1.3e+02ms

# Récapitulatif Temps d'exécution :
# Temps d'exécution Extraction : 104.555223ms
# Temps d'exécution Formatage : 5.328274ms
# Temps d'exécution Insertion onglet 3 : 0.186075ms
# Temps d'exécution Insertion onglet 4 : 1.752293ms
# Temps d'exécution Total : 111.82ms

            # displaying the DataFrame
    # print(df_onglet_1)
    # print(list(df_onglet_1.columns.values))
    # print(df_onglet_2)
    # print(list(df_onglet_2.columns.values))
    # print(df_onglet_3)
    # print(list(df_onglet_3.columns.values))
# value_idcc = [int(v) for v in val_idcc if type(v) == "<class 'numpy.int64'>"]