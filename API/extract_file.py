import pandas as pd
from IPython.display import display
from tabulate import tabulate

def extract_data_excel(path, sheet_index=0, header=0, col_index=None, skiprows=None):
    df = pd.read_excel(path, sheet_name=sheet_index, header=header, index_col=col_index, skiprows=skiprows)
    return df


if __name__ == "__main__":
    path = "DATA/Referentiel_copy.xlsx"
    df_index_1 = extract_data_excel(path, 1, header=[3, 4])
    df_index_2 = extract_data_excel(path, 2, header=3)
    df_index_3 = extract_data_excel(path, 3, header=2)

        # displaying the DataFrame
    
    # changement nom colonne sur 2 lvl
    # df_index_1.columns = pd.MultiIndex.from_tuples(df_index_1.columns, names=['Lvl_1','Lvl_2'])
    # print(df_index_1.columns.levels)
    # print(df_index_1.columns.get_level_values(0).values)
    # print(df_index_1.columns.get_level_values(1).values)
    # print(df_index_1.iloc[0:6, 0:5])
    
    # print(df_index_1)
    # print(list(df_index_1.columns.values))
    # print(df_index_2)
    # print(list(df_index_2.columns.values))
    # print(df_index_3)
    # print(list(df_index_3.columns.values))
    
    # print(tabulate(df_index_1, headers = 'keys', tablefmt = 'psql'))
    # display(df_index_1)