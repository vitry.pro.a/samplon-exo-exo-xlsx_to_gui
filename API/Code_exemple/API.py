import pandas as pd
from fastapi import FastAPI
from extract_bdd import sql_to_df, sql_to_df_TCD_sum, sql_to_df_TCD_count
from utile_mysql import connection_mysql

# Démarrage de fast API par commande :
    # uvicorn API:app --reload
app = FastAPI()

@app.get("/")
def read_root():
    return {"Home": "Page d'accueil sur l'API de pivot_table"}
    # {"Home": "Page d'accueil sur le test d'une API"}


# GET /courses | retourne la liste de course
@app.get("/dataframe")
def dataframe_bdd():
    mydb = connection_mysql()
    df = sql_to_df(mydb)
    return df.to_json(orient='records')


@app.get("/dataframe/sum/{pays}")
def read_item(pays: str):
    mydb = connection_mysql()
    df = sql_to_df_TCD_sum(pays, mydb)
    print(df)
    return df.to_json(orient='index')


@app.get("/dataframe/count/{pays}")
def read_item(pays: str):
    mydb = connection_mysql()
    df = sql_to_df_TCD_count(pays, mydb)
    print(df)
    df_json = df.to_json(orient='index')
    print(df_json)
    return df_json