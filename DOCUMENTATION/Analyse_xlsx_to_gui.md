# Analyse du taleau Excel

## Les noms des tables:
Onglet 1: Me lire
Onglet 2: Onglet 2 - global
Onglet 3: Onglet 3 - référentiel NPEC
Onglet 4: Onglet 4 - CPNE-IDCC

## Les noms de colonne:


> Onglet 2 - global

- Code RNCP
Colonne à reporter dans la BDD sous forme de champs
code_rcnp

- Intitulé de la certification
Colonne à reporter dans la BDD sous forme de champs
name_certification

- Certificateur (premier dans la liste, pour liste complète cf. RNCP)
Colonne à reporter dans la BDD sous forme de champs
certificateur

- Libellé du diplôme/titre
Colonne à reporter dans la BDD sous forme de champs
name_diplome_titre

- numéro de CPNE + nom du CPNE
Colonne à reporter dans la BDD sous forme de champs
Champs compliqué à définir


> Onglet 3 - référentiel NPEC

- Code RNCP
Colonne à reporter dans la BDD sous forme de champs
Type: str
code_RNCP

- Libellé de la formation
Colonne à reporter dans la BDD sous forme de champs
Type: str
label_formation

- Certificateur* (*liste non exhaustive - premier dans la fiche RNCP)
Colonne à reporter dans la BDD sous forme de champs
Type: str
certificateur

- Libellé du Diplôme
Colonne à reporter dans la BDD sous forme de champs
Type: str
label_diplome

- Code CPNE
Colonne à reporter dans la BDD sous forme de clef étrangère
Type: int
id_CPNE clef étrangère à onglet 4

- CNPE
Colonne n'est pas à reporter
Type: str
label_CPNE identique à onglet 4

- NPEC final
Colonne à reporter dans la BDD sous forme de champs
Type: int
NPEC_final

- Statut
Colonne à reporter dans la BDD sous forme de champs
Type: int
statut

- Date d'applicabilité des NPEC** (**hors contrats couverts par la valeur d'amorçage + voir onglet "Me lire")
Colonne à reporter dans la BDD sous forme de champs
Type: date
date_of_application_NPEC


> Onglet 4 - CPNE-IDCC

- Code CPNE
Colonne à reporter dans la BDD sous forme de champs
Type: int
id_CPNE

- CPNE
Colonne à reporter dans la BDD sous forme de champs
Type: str
label_CPNE

- IDCC
Colonne à reporter dans la BDD sous forme de champs
Type: int
value_idcc

![MCD](/BDD/MCD/MCD_xlsx_to_gui.png)

## Modélisation de la structure de la BDD
----------------------------------------------------
    > Onglet 4 - CPNE-IDCC
- Table: CPNE
    Caractéristique:
    - code_CPNE ou id_CPNE : int 
    - label_CPNE : str

- Table: IDCC
    Caractéristique:
    - id_IDCC : int
    - code_CPNE: int
    - IDCC : int

----------------------------------------------------
    > Onglet 3 - référentiel NPEC
- Table: RNCP
    Caractéristique:
    - code_RNCP ou id_RNCP : int
    - label_RNCP : str
    - id_certificateur : int
    - id_diplome : int

- Table: Certificateur
    Caractéristique:
    - id_certificateur : int
    - certificateur : str

- Table: Diplome
    Caractéristique:
    - id_diplome : int
    - label_diplome : str

- Table de relation: RNCP_CPNE
    Caractéristique:
    - code_RNCP : int
    - code_CPNE : int
    - NPEC_final ou "Niveau_de_Prise_En_Charge_final" : int
    - statut : str
    - date_of_application_NPEC : date

---------------------------------------------------
    > Onglet 2 - global
- Table: "Niveau_de_Prise_En_Charge" NPCE
    Caractéristique:
    - id_NPCE : int
    - code_RNCP : int
    - code_CPNE : int
    - value_NPCE : int

__________________________________________________

![MCD](/BDD/MLD/MLD_schema_table_xlsx_to_gui.png "MCD")

___________________________________________________
Data:

> Onglet 2
- Code RNCP / code_rcnp
- Intitulé de la certification / name_certification
- Certificateur / certificateur
- Libellé du diplôme/titre / name_diplome_titre
- numéro de CPNE + nom du CPNE / Champs compliqué à définir

> Onglet 3 - référentiel NPEC
- Code RNCP / code_RNCP
- Libellé de la formation / label_formation
- Certificateur / certificateur
- Libellé du Diplôme / label_diplome
- Code CPNE  / id_CPNE
- CNPE / label_CPNE
- NPEC final / NPEC_final
- Statut / statut
- Date d'applicabilité des NPEC / date_of_application_NPEC

> Onglet 4 - CPNE-IDCC
- Code CPNE / id_CPNE
- CPNE / label_CPNE
- IDCC / value_idcc


![MLD](/BDD/MLD/MLD_V1_xlsx_to_gui.png "MLD_V1")
![MLD](/BDD/MLD/MLD_V2_xlsx_to_gui.png "MLD_V2")